**Note:** These instructions assume you are setting up GitLab as an SSO client to https://courses.opencraft.com.

# Usage instructions
1. Install ansible-playbook, if you haven't already. 
   See [this page](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) for installation instructions.
2. Make sure you have a valid domain/subdomain pointing to the server you want to install Gitlab as it will be used to 
   generate SSL certificates
3. In the `ansible` directory, create a folder called `host_vars`
4. Copy `domain.yml.example` into the created `host_vars` directory and rename it to `<domain>.yml`. `<domain>` is the domain
   you have pointed to your server. Fill in the missing values.
   For this project, they are located under Settings > CI/CD > Variables.
5. Fill in the variables. Note: `edx_lms_client_id` and `edx_lms_client_secret` need to be obtained from edx LMS. An 
   Oauth2 application might have to be created if it doesn't exist already. When creating the Oauth2 application, remember
   to add `https://<domain>/users/auth/oauth2_generic/callback` to the redirect URIs.
6. In a shell, run `ansible-playbook -i hosts --private-key <KEY> playbooks/gitlab.yml`. `<KEY>` is the private key you
   are using to access the server you have set up.
7. Check gitlab is installed by going to `https://<domain>
 
