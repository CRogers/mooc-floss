# Brainstorming Sessions - Notes

Archive of the Framapad: https://mensuel.framapad.org/p/mooc-floss-0-9lpt

(detailed) Course structure 


W1 - ?
1.1 Introduction to collaboration - A recreational approach
1.1.1 Video : What we will do here and why
1.1.2 text tutorial : How we will do it (Minetest intro / rules)
1.1.3 activity: DO IT
1.1.4 debrief (video or texte) - here is what you has as an experience .. (it's difficult to choose what to do, diffuclté it's difficult to know how to do it, it's difficult to interact) - do the parallel with software dev.
1.2 People talking about their first contributions (video interviews?)
1.3 FLOSS theory
1.4 Give some love to your favourite floss projects
1.5 Talk about the Project - tips to measure the health of a project ?




 ---- Brainstorming sessions (Feb 2021) ----


Feb 10th - About the MOOC in general

### Agenda

All topics are thought to be a presentation (of the work already done) and an open discussion

 * Introductions from 18h30

- Marc Jeanmougin: Research Engineer at Telecom Paris. Want to make a MOOC on FLOSS. Contributor to Inkscape. 
- Rémi SHARROCK: Researcher and lecturer at Télécom Paris - already did quite a lot of great MOOCs in the past (C language, linux, etc)
- Polyedre: student at Bordeaux in CS, intern at Paris, future tester of the MOOC \o/
- Jérémy Babin: Bordeaux also Web Developer (php/nodejs/angular....), git teacher and gdpr teacher in my web society to my collegues and client, interrest to MOOC for share my teach.
- Charline : not an IT person, in Bordeaux also, FLOSS supporter and working at becoming a (digital) learning engineer; here to check if I can be of any help compared to my profile
- Xavier Antoviaque: @Opencraft, works on openEdx - already plans courses on onboarding people into floss and openedx - already participated in upstream university (more like mentoring 1-to-1 people into projects)
- Olivier Berger (aka obergix), with a long profile on FLOSS, and also working on a MOOC on Git

 * Why this MOOC (goal, audience, rough contents)
 * Course outline, length, and proposed structure

Week 3 : insist on "how to find a project that accepts contributions"

    "partner" with projects (and give the list) who have a good onboarding process


Give information about the bold steps as soon as the beginning of the course and make it clear that it's not an *easy* task, so that people can think about it and do their research

(todo : mention how to assess whether a project is a good fit (time to answer, time to PR, etc (presence of CI, tests, etc )) + how to research on the history of the project )

Questions for week 3: 
    When was last activity ?
    How to contact the maintainer(s) ?
    
* How to contribute
     gitlab
     
 * Technical choices
  what source in the repository ? 
 
 * Questions
end 20h

governance 
types of contributions : 
    partners 

-> give ways to "fast-forward" modules if people are already familiar with the subject



Feb 11th - Brainstorming week 1

The goal of this week is to discuss FLOSS in general. In particular

    Some history

    The 4 Freedoms (of course)

    Benefits

    Free Software vs Open Source Software, ethics


Agenda:
    
###  Introductions

- Marc Jeanmougin: Research Engineer at Telecom Paris. Want to make a MOOC on FLOSS. Contributor to Inkscape. 
- Rémi SHARROCK: Researcher and lecturer at Télécom Paris - already did quite a lot of great MOOCs in the past (C language, linux, etc)
- Polyedre: student at Bordeaux in CS, intern at Paris, future tester of the MOOC \o/
- Xavier Antoviaque: @Opencraft, works on openEdx - already plans courses on onboarding people into floss and openedx - already participated in upstream university (more like mentoring 1-to-1 people into projects)


- What are the cultural things people wanting to interact with FLOSS projects will need to know ? Without going into too much details, maybe, as much will be discussed in week3 (and where do we put the limit between wk1 and wk3?
- What are the main ideas we want to convey in this module?/
- Do we do video interviews or presentations, and if so, who, and which questions?/
- What external resources could/should we use? Which ones do we link to?
- How to introduce the "main project" to the students?
- How to put this into "blocks"?
- MCQs (during the different blocks, and at evaluation)


Week 0 ? (to be discussed later)

Suggestion: adding a "Why Floss ?" and example projects and initiatives
why choosing floss and why this mooc is relevant/important because floss is a great tool
why floss is good for
- society
- humanity
- ethics
- education

why floss for small companies/big/independant etc..

free software can improve society/ changing the world
FLOSS is a common good = communism / community / interaction / efficient way to create a good an reliable software 
good reasons of using floss
Explain why it's important to not only use free software, but also to contribute upstream (to be empowered using/changing the software)
why is it good for companies to contribute to FLOSS ?  
the story of richard stallman writing printer driver
introducing the open source : eric saint raymond / cathedral and the bazar, bazar is a god way to DO stuff
how to keep / be interesting from the very start ?! interaction ?
starting with an interview of someone very important to floss / famous people
richard stallman (controversial)
Karen Sandler
linus torvalds
microsoft / google? open source
John_Carmack ? Jeff Atwood ?
interview, what questions:
    - advice for developers / for our students 
    - their storytelling 
    - how was it to contribute for you ? easy difficult, why ?  what did you do exaclty ?  how you began contributing ?
    - 
    sharing the "fear/shyness" to start contributing 
    publicly doing something is always difficult
    twitter: the shitstorm metaphor !
    pick your favorite project and say thanks ! <- nice call to action (but kind of depends on week4 : "where do I go ?") ; tweet your favorite project and say thanks; https://fsfe.org/activities/ilovefs/
Keep in the repo a database (csv) of FLOSS projects - to document maybe later stuff like

    Where to find them ?

    easy to contribute ? / accepts contributions ?

    contributions generated by the mooc ?


    Find Open Source By Searching, Browsing and Combining 7,000 Topics (awesomeopensource.com)

    cornelius/awesome-open-source: A curated list of resources related to how to do open source projects (github.com) 

    alternative to 

    f droid

https://www.gnu.org/philosophy/philosophy.html


    upstream university: using legos, someone contruct a piece and someone else is building up 



    warning: goes both ways, you might be prepared not ta take personally some remarks ! 
value: 

games to understand issues :
    minecraft - it's great to be many to build a cathedral 
    



Example of structure of the week: 
    - 
    - The free software movement
    - Open source 
    - « FLOSS » as a neutral term / compromise
    - …
    - At the end of this mooc, you will be expected to contribute to a floss project, please startt thinking about it


Feb 15th - week 2 - git/gitlab/github

### Introductions

- Marc Jeanmougin: Research Engineer at Telecom Paris. Want to make a MOOC on FLOSS. Contributor to Inkscape. 
- Rémi SHARROCK: Researcher and lecturer at Télécom Paris - already did quite a lot of great MOOCs in the past (C language, linux, etc)
- Polyedre: student at Bordeaux in CS, intern at Paris, future tester of the MOOC \o/
- Xavier Antoviaque: @Opencraft, works on openEdx - already plans courses on onboarding people into floss and openedx - already participated in upstream university (more like mentoring 1-to-1 people into projects)
- Olivier Berger: Research Engineer at Telecom SudParis. Long-timer in Free Softawre communities, is creating a MOOC on Git, in French

### Scope : 
    Prerequisites? 

        Commandline?

        Git ?



Resources: 
    https://guides.github.com
    something like https://chris.beams.io/posts/git-commit/
    
gitlab integrated for those that want to contribute ?
contribute to floss project specifically to CODE contributions
git in the command line is really generic, rather than a UI in top of it
GIT MOOC with Olivier Berger Telecom SudParis: prerequisite?
better to focus on concepts like : tree,merge request,fork,clone,archeology rather than command line 
https://blinry.itch.io/oh-my-git
https://learngitbranching.js.org/
(nothing to do with this module but I put it here):
Why do People Give Up FLOSSing? A Study of Contributor Disengagement in Open Source https://cmustrudel.github.io/papers/miller19dropout.pdf
(end of nothing to do with this module)
git workflow
code reviews process
automatic quality verification
how was it before github/gitlab ? 
problem of "no maintainer" / fuzzyness: many copies with random patches
recognize the project activity

become a FLOSS maintainer (next level): recreate a community by yourself (it's another mooc :)

git platforms: flowchart

should we exclude projects that do not use git ?

flowchart with many questions on each step 

be aware that the timeline could be really long !
SYNCHRONOUS vs ASYNCHRONOUS 

LTI / omniauth stuff : 
https://github.com/xaviaracil/omniauth-lti + https://docs.gitlab.com/ee/integration/omniauth.html#using-custom-omniauth-providers

Skills to teach:
    - where to find stuff (find the repostory, find the branch, find the specific version, tags, etc)
    - fork
    - 



Feb 18th - week 3 : "Who?" - Orgs, Licenses, and economic models


  Sketch of agenda:

    Introductions

    What are the main ideas we want to convey in this module?

    Is this just an example of "successful" projects?

    Do we do video interviews or presentations, and if so, who, and which questions?

    What external resources could/should we use? Which ones do we link to?

    How to put this into "blocks"?

    MCQs (during the different blocks, and at evaluation)






Introductions:
- Marc Jeanmougin: Research Engineer at Telecom Paris. Want to make a MOOC on FLOSS. Contributor to Inkscape. 
- Rémi SHARROCK: Researcher and lecturer at Télécom Paris - already did quite a lot of great MOOCs in the past (C language, linux, etc)
- Polyedre: student at Bordeaux in CS, intern at Paris, future tester of the MOOC \o/
- Xavier Antoviaque: @Opencraft, works on openEdx - already plans courses on onboarding people into floss and openedx - already participated in upstream university (more like mentoring 1-to-1 people into projects)
- Olivier Berger: Research Engineer at Telecom SudParis. Long-timer in Free Software communities, is creating a MOOC on Git, in French
- Loic Dachary: Long-timer in Free Software communities, created an upstreaming program a few years ago, still living in the Openstack project : https://docs.openstack.org/upstream-training/
- Anna Khazina: MOOC program manager at IMT, helps MOOCS projects

≠ between "projects" and "orgs".

List of communities:
    - FSF (FSFE, FSFF)
    - Framasoft (France)
    - KDE, GNOME, Red Hat, Canonical, Gitlab
    - Debian
    - Microsoft ? (Github, VS Code, etc)
    - OpenStack ?
    - Cloud Native  Computing Foundation https://www.cncf.io
    - Google
    - Languages (Python, Rust, etc)
    - Eclipse (now/soon European association registered in Brussels)
    - SPI / Software Conservancy


Let's define vocabulary (may be interesting to students too ;) https://www.wikidata.org/wiki/Wikidata:WikiProject_Informatics/FLOSS can be used as a good base (ontology)
 - project
 - community (users, contributors)
 - organization (non profits) ?
 - companies (some type of organization?)
- fiduciary umbrellas (Linux Foundation, SFC,  ...)
- informal group

Activity idea: 
    - given a project, find names or orgs, etc
    - for your chosen project, find info about its community
    - Look on github or gitlab what are the most famous projects ?

another activity : contribute to wikipedia/wikidata 
https://en.wikipedia.org/wiki/Wikipedia:Contributing_to_Wikipedia 
screenshot of what they did on wikipedia
plan iterations in activities :
    - edit WikiPedia on whatever, to get trained to process and general contribution idea (early weeks(could even be in week 1, after the onboarding game)) : learn about contributing and what happens with review, etc. (high chances of discarded by reviewers)
    - editing something specifically for week 3 : more focused and more valuable (add a relevant, single, sourced, small bit of information)

What external resources could/should we use? Which ones do we link to?
  -> mooc on wp contribution ?
https://www.fun-mooc.fr/courses/WMFr/86001S02/session02/about  

(I don't find anything on edx or coursera about wikipedia (not wordpress), the most related things were
https://www.coursera.org/lecture/friends-money-bytes/wikipedia-and-consensus-formation-AAwAX
https://www.coursera.org/lecture/strategic-innovation-building-and-sustaining-innovative-organizations/the-case-of-wikipedia-c555e
)

interview - expert on the licence topic : 

talk about the troll topic 
the contributor you want to avoid
sociological contribution 
be prepare to act in front of a troll
https://media.ccc.de/v/36c3-10875-hack_curio  

how does the licence or the contribution guide affects the process ?

https://choosealicense.com/
https://creativecommons.org/choose/


compare what the stated goal of a project are, and what the project chooses
https://www.internetactu.net/2021/02/17/regouverner-1-2-la-nouvelle-ere-des-licences-libres/

Business model: a concept around "scarcity"
  - Saas
  - Services: support, mentoring or development (e.g. RedHat)
  - relicensing (e.g. dual-licensing AGPL/proprietary)
  - open core

basically : 
    1/ talk about the basics, "historical" business models
    2/ talk about the licenses and what they imply, pitfalls, CLAs
    2bis : talk about *motivations* (of contributors, and of orgs)
    3/ activities
    4/ recent developments, clouds, maybe advanced topics

Resource (I have to add resource I got from FSFE)


Questionnaire in the beginning of the course adressing expectations

(link from Olivier : 
https://www.enricozini.org/blog/2015/debian/standup-comedy-notes/ )


Feb 22nd: Contribution


- Marc Jeanmougin: Research Engineer at Telecom Paris. Want to make a MOOC on FLOSS. Contributor to Inkscape. 
- Rémi SHARROCK: Researcher and lecturer at Télécom Paris - already did quite a lot of great MOOCs in the past (C language, linux, etc)
- Polyedre: student at Bordeaux in CS, intern at Paris, future tester of the MOOC \o/
- Xavier Antoviaque: @Opencraft, works on openEdx - already plans courses on onboarding people into floss and openedx - already participated in upstream university (more like mentoring 1-to-1 people into projects)
- Loic Dachary: Long-timer in Free Software communities, created an upstreaming program a few years ago, still living in the Openstack project : https://docs.openstack.org/upstream-training/

What should we convey in this week of the MOOC?

Loic:  convey the idea that the main obstacle to contribution is social and not "code".
Even if you're a driveby contributor, you'll need to understand what the reviewer of your code will expect, what they will ask of you, etc

Loic: as soon as you find people, go ASK A QUESTION. Does not matter which one. (What matters is that you engage the conversation)

interact ASAP, because you will need to be known from people when you'll submit your MR. If they know you for a longer time, it will be easier as the people will know you, 

It will also be easier if you know people to align expectations (you will know more about the scope of the project, and about the expertise of people present in the project)

How to convince potential contributors it's worth their time (interacting) ?

 - The social part of contribution is important because of *netrworking* in general
 - Your contribution time is not the only thing involved in a successful contribution : There is also time spent by reviewers, by people maintaining or updating the code, etc. 


How to find the discussion channels where people interact ?
- identifying PLACES and ROLES is important
- you'll (probably) need to ask questions to get into the codebase and find where the stuff you'll fix it, then discuss how to get things accepted.

lvls of contrib:
 answer a question
 fix a typo in the doc
 fix a trivial bug
 ...
 reorganize the governance of the project (you're now de facto in the governance core team)

They are "already" ordered by *social* time to interact


Code of conducts are basically the social rules to interact and they set expectations
Code of conducts are the last resort when people have issues: 


Examples : avoid de-anonymizable examples. Maybe mention the Linus case where he admitted that social issues *are* an issue

"newcomers" can be easy "targets" for abusive behaviors, so it's important to recognize those situations.

When entering a project, you are not in "your" world (see week1 game) - you have to "submit" yourself to the reviewers and "accept" that there are reviews (which may vex you or that you may think pointless -- you will score points by following the rules). Parallel with a party: you're at someone's place to take part in the party, you'll follow the host rules until you're a long-time contributor :)  

feedback about the atmosphere of a community is a contribution.



Links : 
    - Role transition in floss communities:  https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig1/AS:961423358304278@1606232599177/Role-transition-in-FLOSS-communities-From-Glott-et-al-2011_W640.jpg
    - Workflow net for novice during initiation phase: https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig3/AS:961423358304279@1606232599420/Workflow-net-for-novice-during-the-initiation-phase_W640.jpg
    - https://www.researchgate.net/publication/41618542_Towards_a_theory_on_the_sustainability_and_performance_of_FLOSS_communities/figures?lo=1
    - About retention in projects: https://ieeexplore.ieee.org/abstract/document/8811892?casa_token=A79_i5pOZ1IAAAAA:QLO11ijN2Riu1qoRjAHivemeffo1FgcjfEQ3ncBkbHjTPZwlKROwg93JVOVot9xpIiFFAXA


Feb 25th: Tools

Introductions:
- Marc Jeanmougin: Research Engineer at Telecom Paris. Want to make a MOOC on FLOSS. Contributor to Inkscape. 
- Rémi SHARROCK: Researcher and lecturer at Télécom Paris - already did quite a lot of great MOOCs in the past (C language, linux, etc)
- Polyedre: student at Bordeaux in CS, intern at Paris, future tester of the MOOC \o/
- Xavier Antoviaque: @Opencraft, works on openEdx - already plans courses on onboarding people into floss and openedx - already participated in upstream university (more like mentoring 1-to-1 people into projects)
- Loic Dachary: Long-timer in Free Software communities, created an upstreaming program a few years ago, still living in the Openstack project : https://docs.openstack.org/upstream-training/
- Anna Khazina: MOOC program manager at IMT, helps MOOCS projects

/!\ make sure the learners are aware of the technical setup they *need* to have to contribute early on

Issue: Many tools are domain or language-specific, how do we make a language-agnostic way ?

-> Peer review ?

-> Explain what you have to be looking for in terms of "setup your environment" pages, and not try to explain people how to get a dev environment "in general"
-> Also explain what are debuggers

Prerequisite : linux command line ? Diff, VCS, difference between unit test and integration test, code cov

How to deal with instructions that do *not* work -> social skills
-> "how to ask questions the smart way"

in "FIND A PROJECT" make sure you understand the terms and what they imply in terms of tooling and of languages

Make a checklist on how to build a project ? 

Global Steps of the mooc:
- Get the code (week 2)
- build it! (week 5)
- Run the tests before modifications because at the end we want them to be still green (week 5)
- Write a modification (week 6)
- Send the modification to the project (pull-request, mail, etc) (week 6-7)


Technical prérequisites : a computer with internet connection, some storage space - the details will depend on the project the learner will be working on (need to phrase that correctly as a pre-requisite for the course)

Evaluation : peer evaluation in cohorts ?  


2 activities  ? the "MR" and the "screenshot proving you've run the code"





March 1st: Big codebases

Parallel with a city : (gource.io -> things are constantly evolving with several people and most people only touch small fractions of the codebase)
    - don't be afraid, there are lots of things
    - you don't have to understand every little detail to make sense of a part
    - use "maps" (docs) -> if there is an architecture overview, READ IT. TWICE.
    - IDEs: VSCodium ? Eclipse too
    - USE YOUR VERSION CONTROL SYSTEM (git log, blame, show, bisect) and know how to find the discussions around a commit in the forge


https://cauldron.io/ Bitergia's project analytics
https://beyondgrep.com/ "ack" -> "better than grep"


Concept of modularity : stuff is not in single files anymore
Concept of debugger, and breakpoint ?
-> non-reproducible bugs are hard. Don't touch them. 

exploration methods : 
    1/ backtrace bugs : you know when to breakpoint, and you will see the exact calls that trigger the bug
    2/ read the code. document things (where needed), or small refactoring
    3/ Give a (honest) try to fixing a bug. Even if it's not the best way at first, activity on a bug may get people who know how to help to direct you in the right direction
    https://en.wikipedia.org/wiki/Rubber_duck_debugging

Look at project roadmaps to see what direction the project wants to take
Don't try to change *everything*. Tinker with things, and start small with the goal of having it merged.

convey the idea that you must be patient, and be active during the compile/build time 

create/use a taxonomy of bugs ?

Rémi : fallback if people "fail" to fix a bug ? no but keep a list of Qs to make sure you're on the right track

ack : grep-like program specifically for large source trees
 Ack is designed as an alternative for 99% of the uses of grep. ack is
 intelligent about the files it searches. It knows about certain file
 types, based on both the extension on the file and, in some cases, the
 contents of the file.
 .
 Ack ignores backup files and files under CVS and .svn directories. It
 also highlights matches to help you see where the match was. Ack uses
 perl regular expressions.

live twitch hacking, for participants to learn, compare approaches, find patterns, etc.



2021-03-29:

[19:21] Marc Jeanmougin : https://mensuel.framapad.org/p/mooc-floss-0-9lpt?lang=fr
[19:30] Marc Jeanmougin : https://lite.framacalc.org/9ml5-mooc-floss
[19:35] Marc Jeanmougin : https://gitlab.com/mooc-floss/mooc-floss/-/blob/master/syllabus/transcripts/brainstorm-module-1.md
[19:41] Ildiko Vancsa : https://www.minetest.net/
[20:02] Rémi SHARROCK : I also found something like https://steamrealm.margonem.com/ in the browser, MMORPG
[20:02] Rémi SHARROCK : but I didn't find anything like 2D minecraft in the browser...


