const urls: Record<string, string> = {
  flossDefinition: 'https://en.wikipedia.org/wiki/The_Free_Software_Definition',
  framasoft: 'https://framasoft.org/en/',
  imt: 'https://www.imt.fr/',
  inkscape: 'https://inkscape.org/',
  moocFlossRepo: 'https://gitlab.com/mooc-floss/mooc-floss',
  moocFlossLicence: 'https://gitlab.com/mooc-floss/mooc-floss/-/blob/master/LICENSE.md',
  moocFlossContributors: 'https://gitlab.com/mooc-floss/mooc-floss/-/graphs/master',
  openCraft: 'https://opencraft.com/',
  openEdx: 'https://openedx.org/',
  openStack: 'https://www.openstack.org/',
  openInfrastructureFoundation: 'https://openinfra.dev/',
  patrickAndLinaDrahiFoundation: 'https://plfa.info/',
  publicityPod: 'https://www.flickr.com/photos/publicitypod/',
  telecomParis: 'https://www.telecom-paris.fr/',
  mailingList: 'https://lists.opendev.org/mailman3/lists/floss-mooc.lists.opendev.org/',
  betaCourse: 'https://courses.opencraft.com/courses/course-v1:MOOC-FLOSS+101+2021_1/course/',
};

export {urls};
