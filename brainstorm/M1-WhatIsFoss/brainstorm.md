**WARNING**: The content of this file has been moved to the course itself, in `../course`. Don't edit the current file -- instead, make changes to the course itself.

# Module 1 - Brainstorm, Sorted by Section

* Module coordinator: Xavier
* Length: ~2h

* We take a very practical approach -- contribution isn't something you can learn just in theory, it's something that has to be done and experienced to be understood. Until you've actually contributed your code to a project, felt the sweat of pressing the submit button, and achieved the thrill of seeing it merged by a project maintainer, it's difficult to actually understand, and remains a nebulous and often frightening prospect.
* We are going to take you, little by little, through the various steps that lead to successful contributions. But you have to do your part, and actually perform the activities presented throughout the course. Just like in any open source project, there will be a world of difference between the doers, the ones who actually contribute some work, and the lurkers, who only watch, and never fully join a project.

## Minetest

Assignees: Marc & Rémi

* Introduction to collaboration through a game:
  * Video presenting why we do that: to generate an interaction between learners: "I create something, you come to add to it," to show some of the very basic mecanisms of contributions (and receiving contributions) without getting too technical.
  * Then sending them into the game to contribute something there, giving them a goal like building a house, turning a house into a palace, or changing the decoration there -- in a way that fits within the environment. And maybe come back later, to see how others altered the creation.
    * Include a good tutorial, a video of someone going through the steps of playing. We want to be inclusive of people who are not familiar with 3D video games, and make sure they don't feel excluded from the course right away. For those who have never played a video game with WSQD & a mouse, it's not going to be straightforward. For example, the default tool when joining is destroying blocks in front of the player -- the default action is thus to destroy everything rather than constructing something.
    * Handle the asynchronous aspect - there might or might not be other learners in the game when one joins it. Provide instructions for both situations.
    * Provide a way to report abuses & bad behavior: report it on the forum. Also refer to the code of conduct of the course.
  * Then a recap video: "Here is what you lived in this world. Here is what it means what you will live in the real world."
* The game is used to draw a few parallels with open-source collaboration:
  * Learners come in a world that already exists with already existing creations in it - like when you come in a business project. Learners have to fit in and contribute things that fit into the existing universe. It teaches that this universe is not ours as a new contributor, we're entering it, and discovering it as we edit it. When we come in to a building world, we're not sure what we want to build or what other people want us to build. There are people building a house here, "How do I fit in? How do I discuss with those people?" etc.
  * See the perspective from both sides of a contribution: as the one making a contribution, and as the one receiving a contribution. To also put learners in the position of the maintainers they will have to interact with, who created something that they want to modify. To see how it feels to have someone coming to make changes they would not have expected on top of their own vision for their creation. To see concretely that, when someone comes and tries to change something we have created, we won't necessarily like that change, especially without communication and collaboration between the original creator and the new contributor. There needs to be a process and limits to that, we can't expect maintainers to just accept anything.
    * One of the big mistakes that a lot of people do when they first contribute to FLOS is to think that, because we come to contribute something voluntarily, that this is automatically something that the project should be thankful for, and bend sideways to accept and merge in, without requiring  additional efforts from the contributor. Just sending a pull request and expecting to be done isn't going to cut it -- it's the beginning of the process, with the goal being to adapt the contribution in a way that makes it acceptable to both the contributor _and_ the upstream maintainer.
    * Relatedly, on many public Minecraft servers, there is a process to contribute -- otherwise anyone can come and destroy the work of others, or alter it in undesirable ways, as the game will likely illustrate. The issues are similar to contributions in FLOSS: permissions, trust.
    * More broadly, the necessity to handle unwanted behavior in general will show up. We can use the opportunity to mention the code of conduct of the course again, walk through its points, and note that the need to adhere to it is valid everywhere the course points to, within the course (game, forums) or outside of it, on the projects being contributed to (which might also have their own code of conduct, which should also be read and followed).
  * On the other side, even if aggregrating contributions isn't always simple, the game will help to highlight the fact that 10 people contributing to build a cathedral in Minecraft goes much faster than someone doing it alone. It makes the principles of contributions to a common good more concrete, even before entering the more purely technical aspects specific to code contributions.
  * The question of the types of communication channels will likely also come up, because people will probably chat in the game, but it's not as efficient as setting a parallel communication channel. There might be also moments where other people are around, and then we can have something synchronous, but sometimes not. This also reflects what happens in FLOSS projects. Sometimes we get immediate response; sometimes we don't. Sometimes we are just working on our own and trying to fit our pieces in, without a whole lot of feedback until someone else comes in, sometimes days or weeks later. Other times, we're trying to place a piece and there are hundreds of other people trying to change the same thing at the same time.
* There are differences with "real world" contributions too, for example in the video game by default, there are no permissions, what is done is immediately merged, rather than going through a system of merge requests, reviews, etc.
* The stakes are also low: people just create something that they care about for 15 minutes. But it's not something on which there is a deep need to have something curated, or have good moderation.

Inspiration comes from Upstream University: using legos, someone was constructing an object/structure, and someone else came with an idea of how to modify it.

Further references:
* https://gitlab.com/mooc-floss/mooc-floss/-/issues/14
* https://gitlab.com/mooc-floss/mooc-floss/-/issues/15
* https://gitlab.com/mooc-floss/mooc-floss/-/issues/27

### Introduction to the activity (video, 3 min)

Welcome to this first course activity, which will be about « Collaboration ». 

(Fade to gource video)
In Free, Libre and Open Source software, people try to work collaborately on projects. Sometimes in small teams, sometimes in larger groups, and sometimes with thousands or tens of thousand people. To this day, the linux kernel has seen contributions from more than 21000 different people! Some just contributed to fix a typo, and some made major changes that shaped the project to what it is now. 

We will start this introduction with a comparison between development and a completely different field: video games. More precisely, "sandbox" video games allow players some creative freedom and a multiplayer potential that we will use to draw parallels between the play experience, and some of the things you'll go through during your contribution experience. 

(fade to Minetest gameplay)
Here we will use Minetest, which is similar to a well-known game, but is an open-source project, so it is possible to contribute to it and help it to improve in future versions. We will provide you a world to build in, and your goal will be similar to the course's goal in open-source software: Improve something. You can build a new building, add decoration or improve the design of something already in the world, you will just need to make sure you are leaving the world a better place than what it was when you entered it. Then come and brag about what you improved in the forum!

(end of video)

(text)
<caveat> This activity is *recommended* but ultimately optional and there are very good reasons for which you would like to avoid it: If you're unfamiliar with 3d games, and do not already know how to move around with WASD keys and mouse, you might get frustrated at this activity, or if you do not have a mouse (It's technically possible to play with a touchpad for instance, but the experience will be affected). The goal of this activity is to make you understand how collaboration organically emerges in the right environment, and what can stem from a creative open-ended collective, so if you think you would not benefit from it, you can skip to the debrief video.</caveat>

### Installing & configuring Minetest

#### Prerequisites (text/images, 1min)

Minetest should work on any hardware and with most operating systems (Windows,
macOS, or most linux distributions) and even on some Android phones and tablets. 

If you are in a restricted environment, like a school or university, it is possible that the network blocks some ports, including the default port of Minetest. In this case, ask your system administrator to unlock port UDP 3000.
TODO (Mc): see if we can host on port tcp/443 

#### How to install minetest (text/images, 2 min)

You can find installation instructions at https://www.minetest.net/downloads/

#### Activity (text/images, 7 min)

Once installed, you can launch it and try a test world: TODO Screenshots
 - Click on "New" to create a new world, then click on "Create".
 - Select "Creative mode" on the left, then click on "Play game"
 - Check that you can move around with WASD keys, familiarize yourself with the
   mouse movements, with what happens when you left click, look at the block
types in inventory (<key>I</key>), try to place one with the right click.
 - Once you're comfortable with the keys, press Esc, then Exit to menu.
 - In the menu, click on "Join game" then enter TODO and TODO in Address/port fields.
 - First, "visit" what is already there. You may get ideas of the underlying
   logic of what is around already.
 - If there are other people in the server, great! You can take this
   opportunity to ask them (press <key>T</key> to chat) what they are building and
how to help them!
 - If you're alone, no worries. Just look around, and find something to improve
   on, or to add to the world.
 - You can spend as much time as you want on this project, but we recommend to
   try for at least half an hour, then post on the forum with a screenshot of
what you've done :)


#### Troubleshooting

 * If you don't have a QWERTY keyboard layout, you might need to adjust keys, which
you can do in the menu (Esc →  "Change keys").

 * If you find instances of unacceptable behaviour (chat abuse in violation of
   the code of conduct, etc.) please report it on the forum and we will act
   accordingly. 

 * If the server is full, please try again later and report the issue on the
   forum, we'll try to increase the capacity


### Experience your first collaboration	(20-30min)

Now is time

TODO (Mc): setup/initialize a server + Choose game mode (Dreambuilder ?)


### Debrief	(video, 5 min)

(Video - minetest play)

Welcome back! We hope you had a fun time. Now let's debrief: On purpose, we've sent you into an unknown world, with unclear instructions, and lots of creative freedom. Which created a first question: *What should I do?*

This is a great first question, and once that is central to this course. The aim here is to get you to contribute to a free software community project, and the first question you should ask yourself is indeed this: What should I do? 

The **What could I do?** question comes next. In the game, you *could* do anything, but the wording of the task, *leave the world a better place*, is context-dependent. Is there something that, on the first glance, looks bad and need improving ? Is there some unfinished thing in the middle of the place that you can contribute to ? Then you found your task. In a free software project, you will find bug lists and roadmaps that will show you a direction. You will also find dead code, undocumented unclear and untested code, code that is slow and unefficient, and all that could be improved.

**How to do it?** is obviously a matter of perspective. You will have to use the tools you know, your skills and your knowledge. In some cases, you won't know. And that's okay! Usually, the best way to figure out how a community expects to do something is through discussions. 

And, when you have several people focused on a common goal, whether in a game, or in any situation, communication, and at some point people skill in general, are key. If you were ingame with several people, maybe you found yourselves using the chat, and maybe you found yourselves looking for a most efficient way to communicate, as those questions tend to arise naturally in any collaboration environment. 


### Minetest TODO 
* discuss with framasoft
* It can be useful to let people know that they can skip this if it doesn't work out for them, but I would encourage more strongly to try it out first, rather than telling them they can skip if they don't think they will benefit from it. Being unfamiliar with 3d games adds difficulty, but it is not a good reason to avoid it I think -- they could still learn and end up enjoying it. They won't know if they don't at least try!
* Maybe add a link to the exact system requirements? https://wiki.minetest.net/Minetest#System_requirements
* You can find installation instructions at https://www.minetest.net/downloads/ ->  step-by-step video tutorial, no?
* Once installed, you can launch it and try a test world: TODO Screenshots - Click on "New" to create a new world, then click on "Create". -> Imho a video with a voice over comment would be better than screenshots here, especially if we want to be inclusive of people unfamiliar with 3d games.
* then post on the forum with a screenshot of... -> It would be good to provide more guidance about what to do, and how -- maybe show some examples, of both new creations from scratch, improvements to other people's creations, what to say/ask to others if some are around, etc.
* "Now is time..." -> What is the difference between this part, and what is above? Once they are in game, they are already in one activity no? If the previous activity is meant to be purely about setup and configuration, and this one about the collaboration itself, maybe move the steps from the previous activity from line 76 !22 (diffs) to here?
* It's a good start, but there are a few additional points to be made in this section -- cf the recap from the brainstorm meetings we had: !13 (diffs)




## Starting into FLOSS (video interviews, 5-7 min)

Assignees: Kendall, Xavier

* Try to introduce to the several modules of the course from the interviews
* Approaches suggested:
  * Make >5 min videos and have people watch at least one?
  * Interviews excerpts to be split and spread out throughout the whole course & section. This way instead of one big chunk of long interviews, we get frequent tidbits provided throughout the course (a bit the way interviews are interspersed in video documentaries). Will need to collect questions relevant to all portions of the course to build an interview questionnaire (interviews will thus be better done after we have the detailled structure for the whole course).

### Interviewees

Find variety of people (diversity (on all levels, geographical, language, gender, current occupation(student/independent/gafam/etc), etc), roles, famous-ness):

* RMS
* Linus Torvalds
* Jeff Atwood (discourse, Stack Overflow)
* Karen Sandler (Gnome Foundation, Software Conservancy)
* John Carmack (Quake, Doom)
* Solomon Hykes (Docker)
* Doug Hellman (Python, OpenStack)
* Paris Pittman (Kubernetes)
* Davanum Srinivas (OpenStack, Kubernetes)
* Sage Sharp (Software Freedom Conservancy, Outreachy)
* Aeva Black (Confidential Computing Consortium, Kubernetes, OpenStack)
* Limor Fried (Open Source Hardware)
* Stefano Zacchiroli zack@upsilon.cc
* Mariusz Felisiak (Django, https://twitter.com/MariuszFelisiak)
* Kushal Das (Python, Freedom of the press, https://twitter.com/kushaldas)
* Vaishali Thakkar (Linux Kernel, https://twitter.com/vthakkar_)
* Karl Fogel (Producing Open Source Software book)
* Companies:
  * Microsoft / google - Open source departments

Reference discussion: https://gitlab.com/mooc-floss/mooc-floss/-/issues/12

### Questions/topics

For the current module:

* How did you get involved into FLOSS? - what was it like?
* What do you think about FLOSS communities? About the contributors in the project you have participated in or met?
* How do you think good collaboration happens in software, in general?
* What would you say to new potential FLOSS contributors?
* Tell the story of your first contribution / how you began contributing to a project with a community? (or third-party)
  * What did you do exaclty?
  * How did you choose the project? Why did you do that contribution to it? Were you a user of the project before, and if so when did you consider doing more than using the project?
  * How was it to contribute for you? Easy/difficult, why? Is it easier because of who you are, or do you also have to convince people?
  * How did you feel? Sharing the "fear/shyness" to start contributing
* Impostor syndrome - did/do you feel it? ("If I post that publicly, everyone will see that I'm not such a great coder after all. Free software maintainers guys are way better than me.")
* How is it to do work publicly? To have to put yourself forward, in front of everyone?
  * Twitter: the shitstorm metaphor !
* What are other ways than code that you have contributed to open-source projects, or have appreciated seeing others contribute? (documentation, triaging or filing bugs, speaking at conferences...)



* Questions for interviews for the current section
  * How did you get involved into FLOSS? - what was it like?
  * What would you say to new potential contributors?
  * Impostor syndrome

### Quiz

## FLOSS History (video interviews, 5-10 min?)
 
Assignees: Loic & Obergix?

### The Four Freedoms - Free, Libre & Open Source Software

* The story of richard stallman writing printer driver - it's his printer so he should be able to read and modify the code of the driver.
  * Free software is insisting on how free software can improve society as a whole. And be a better way of collaborating, of producing, and of changing the world.
  * It's about creating a common, in the first sense of the common/communism. A common good. Free software is a common good.

* Link the four freedoms to the experience the learner just had in minetest: using, creating, contributing, getting contributions:
  * Eg: to be fully free to use or improve a creation, you need to be able to copy it, and be able to do what you want with the copies, including modifying it. If the original author is the only one to decide how the object can be used, changed or copied, then it's always going to be limiting your freedom with it, in some way
  * Changing a copy is distinct from changing the original object, or someone else's -- this is about collaborating on one common object, to get to a better result faster. It requires both parties to agree on a common goal, and to put in place methods to work together.

* Introducing open source : eric saint raymond / cathedral and the bazar, bazar is a good way to DO stuff
  * Open-source was sort of coined as a marketing term to sell the ideas of a free software, but to stripped of philosophical elements, in more "practical" terms which would be more easily adoptable by companies in a capitalistic setting. I.e. take the ideas of contribution, of community, of interactions, of sharing stuff, show that it's an efficient way to create good and reliable software.
* « FLOSS » as a neutral term / compromise.
  * There are different way of approaching the concepts. The goal is to be inclusive of the differences between free software and open source, allowing diversity of perspective on the matter.  FLOSS embraces and allows a diversity point of views - regardless of one's religion, political affiliation or economic sensitivities, wherever one comes from, whether one is young, old, whether one's interest is on the business side or more from the development side, whether you're more interested in the concrete result, the philosophy, the impact for the condition of human beings, there is something for everyone to take from FLOSS.
  * Use interviews to represent that diversity, and try to remain as agnostic as possible between the different approaches. I.e., represent the point of view of open source from the perspective that someone from open source would say it, present the point of view of free software from the perspective of someone prefering free software, etc. Same thing from the business and community sides.

References:
* DFSG
* https://www.gnu.org/philosophy/philosophy.html

### Benefits of FLOSS

* Why choosing floss and why this mooc is relevant/important because floss is a great tool
* Why floss is good for: society, humanity, ethics, education

* Free software can improve society/ changing the world
  * FLOSS is a common good = community / interaction / efficient way to create a good an reliable software.  *
* Why floss for small companies/big/independant etc..
* Why is it also important and beneficial for people and companies to contribute to projects?
  * Beyond just using free software, which is already a great step for personal freedom, compared to using proprietary software, it's also about being empowered to change the software we use.
  * Free software: changing society, giving back to make the world a better place,
  * Open source: also very objective advantages, including for companies that behave more egoistically. In terms of maintenance, being able to customize software without having to take care of *all* the maintenance, or being able to pull resources together, instead of having to replicate work done by others,

* Example projects and initiatives
  * Include interviews excerpts on these points, from the different perspectives presented

### Types of licenses & governance

* How that translates into licenses (copyleft or not)
* Types of governance

### Quiz

## Give some love! (text & activity, 15 min?)

Assignee: Rémi

* The goal is to engage with an open source project, but without committing to anything -- simply to help realize that the contributors are humans that we can talk to and you can engage with. This first contact can be a big psychological barrier, so it's important to deal with it early on, before actually attempting to contribute. Otherwise, it can hinder the necessary level of communication for contributing.
* Start with the simplest interaction possible: go say thanks to any FLOSS project you like!
  * Feel how it's actually hard to take the step of communicating publicly with a free software project, even just to say "Hello. Thank you for the software that you've done. I love it." The earlier this is confronted with, the easier it will be to deal with it later on.
  * Don't worry about bothering the project -- it will be a nice change for the maintainer, who are more used to get people coming to complain than to say thanks! Your post will be a breath of fresh air compared to the more common "Oh, this shitty software doesn't work. How do you make it work?"
  * Just check quickly the recent history of the place you're about to post to, to check that the place isn't already overwhelmed with "thank you!" notes -- it is sadly rarely ever the case, though. If you only see a few, or none, go ahead!

* Give a list of list of projects? 
  * Let learners choose freely between a project of their choice, or a suggestion (partner projects, list of projects). We don't want to discourage them from picking their own project, and should encourage them to think about it before supplying the list.
  * Get learners to contribute their findings to the list
  * Also provide a list of partner projects (Open edX, OpenStack), which work with the MOOC and helps ensuring new contributors get what they need - this is useful for those who want a simpler path to a first contribution, regardless of the project, or simply a recommendation/help to choose one.
  * Include a chat room / forum from the MOOC project itself as an option, or as a first less threatening step
* Help detect how to find a communication channel. Prefer a synchronous channel for this if it's available, as it is more direct and less prone to filling people's inboxes, which works well for a simple "thanks!" - but asynchronous channels are fine too.
* Ask learners to link to their post/contribution
  * Keep a database of contributions/posts generated by the mooc? Would be useful to be able to count and browse them quickly, per project/type

* FIXME: Section to merge with the next one (tips for the project)? See the related FIXME at https://gitlab.com/mooc-floss/mooc-floss/-/merge_requests/14/diffs

References/inspiration:
* Wikipedia / Wikidata - Lists of FLOSS projects
* https://awesomeopensource.com https://github.com/cornelius/awesome-open-source: curated list of resources related to how to do open source projects
* https://framalibre.org
* https://alternativeto.net/
* https://f-droid.org/
* https://fsfe.org/activities/ilovefs/

## Tips for the Project (5 min)

Assignee: Xavier

### Starting early & picking a project

* At the end of this MOOC, you will be expected to have merged a contribution to a FLOSS project
* Contributing is *not* an easy or quick task! It can take a long time, and require many interactions and iterations. It's not uncommon to need several weeks, or even months, to get a meaningful contribution merged.
* So don't keep this for the end of the course! You would likely not have the material time to be able contribute to a project by doing it at the last minute. It's important to start applying what we discuss in this course early on, in a specific project. You will have more time to become familiar with it, progress through trial and error, or even to replace it with another project if it doesn't end up being a good fit. 
* If you have already a project in mind that you would like to contribute, great! Personal interest in a given project can be the best source of motivation, though make sure to still assess its health through the criteria listed below. If you have several projects in mind, pick the one which seem the most healthy (see below)
* If not, you can either pick the project you thanked in an earlier section from the current module, or another project from the lists provided there (LINK)
  * Note that the decision of the project you pick to contribute doesn't need to be final at this stage -- don't worry too much about making the "right" choice for now. The important bit here is to experience doing a first contact, and start getting feedback and information ahead of confirming your choice of project, in module 4. Until then, the modules will list tips to help figuring out if the project a good match for you, or whether there might be another one where you will be able to contribute to more easily. Just make sure to start interacting now with at least one project, otherwise you won't have the experience and feedback necessary to take that decision then!

* FIXME: Worth merging with the section "Give some love!"? It could give something like this: 
  * Starting early & picking a project
    * List of projects
    * Assessing a project health
  * Interacting with a project
    * The importance of early and frequent communication
    * Maintainer's time - A scarce resource
    * Starting as small as possible
    * Activity: Give some love & Introducing ourselves

### Assessing a project health

How to see if a project is is a good fit for a first contribution:
* Check that this project has a place to communicate, that people answer. It can be a forum, chat, mailing-list, or simply a ticket system -- what counts is that there is a track record of people discussing and answering there. When we can't find any place, or if most of the posts/issues posted there are left unanswered, it's probably best to pick another project, as this would make contributing very difficult.
* When was the last activity, and how frequently is there activity on the project? This is important, because it reflects how much attention your own contributions and interactions are likely to get -- it would likely be only a small portion of the overall project activity. It can be communication frequency on the place identified on the previous bullet point, but also more tangible activity like code being committed, merge requests, etc. We need a project showing at least some activity every week, and the last activity being no older than 1 week -- though it's best to pick a project with much more activity: many per day, every day.
* How much time, on average, does it take to get an answer (on tickets, pull requests, forum posts, chat...). It's best to aim for a project on which this isn't more than a few days on average, ideally within a day. Otherwise, it's probably best to pick another project, as the turn-around time on our contributions is likely to be too slow to allow to make meaningful progress in a reasonable amount of time.
* Are they welcoming new contributors? Some projects aren't actually looking for new contributors - they can be a company project who only accept code from its employees, are in maintenance mode and don't accept new contributions, for example. A good way is to check whether any documentation exists for new contributors - often in a `CONTRIBUTING.md` file, or in the project developer documentation. Also look for recent merge requests from new contributors, ie their first contribution, to see how the project maintainers react to it. Do they consider it? Accept it? Is there a licensing agreement to sign?
* TODO: Provide examples with links/screenshots of github to show how to see community activity on a project. E.g. https://github.com/django/django/ is frequently modified (screenshot with highlighted "last commit 2 days ago"), and has recently merged pull requests. They also have documentation for how to get involved in the project, and a big list of ways to contribute, including an "easy pickings" list of bugs. Note that github/gitlab is also the topic of module 2, so there is some overlap, and there will be a fair amount of tutorials/screenshots there. It might be worth seeing how module 2 will look like, we might be able to link to it.

If any of these are problematic, try to find an alternative project. Learning to contribute with a project which isn't good at integrating new contributors contributions can be very discouraging. Even after picking your project, if you ever realize that the project isn't reactive or welcoming enough, don't hesitate to change -- there is very litte you can do to fix it as a new contributor, and there are plenty of other FLOSS projects.

### Interacting with a project

#### The importance of early and frequent communication

Communication is one of the most important first steps to contribute to a project

* An open source project is not just the code, it's a community -- even when it only has a single maintainer!
* The first step, often overlooked, is not to try to push code:  it's to understand how the project and its community works, and understand how we can fit in. It's useful to think of it like joining an organization, a party at someone's place, or an online forum. The ability to successfully contribute to the group will depend on how we approach it.
* We are much more likely to be well received by the group when we start by listening/reading to the current discussions, figuring out the group's dynamic and interests, saying hi, offering to help on the existing projects, using the interactions to progressively learn more and ask questions, etc. If we barge in and immediately try to monopolize the conversation with our own project or topic, it will often not match the group's interest, and could end up being ignored or rejected.
* Talking to other people, in public, on a respected project is also something that can be a difficult psychological step - one of the hardest, especially when joining a new project for the first time. We can be afraid of bothering people, of being rejected or ignored. It's important to deal with this feeling now, and pass that step to realize that it's fine. When this is delayed, everything else is also going to be delayed because everything about contributing relies on establishing good communication. It's important to go and put ourselves out there immediately, and start talking to people.
* Module 4 will go more in detail about communication (include link), but the goal here is to get the ball rolling.

#### Maintainer's time - A scarce resource

* Even though we are coming to contribute, the members of the project are also contributors volunteering their time -- and have been doing so for much longer. So they don't owe us anything: we don't pay them for their work. We aren't clients, but fellow community members, and it's important to treat them as such. If anything, at the beginning *we* owe the project contributors a lot: 
  * The time they have spent building the project until we join it,
  * The time they might spend answering and helping us while we learn the ropes -- that's a precious gift, from very skilled developers, which would be expensive if we had to pay for it.
  * The time they spend maintaining the project on an ongoing basis -- including maintaining any patch that they would accept in the project, including our own contributions. It's easy to change code once in an open source project -- but having to carry it over in each release is a burden that the maintainers take over from us when they merge a patch, and that can be a larger commitment than the patch itself.

* The most scarce resource in a FLOSS project is the project maintainers' time. To be able to contribute successfully as a new contributor to the project, we will need to use some of their time -- at least for reviewing our work, and hopefully to get some further guidance and answers while we learn. To get the maintainers to prioritize spending some of their time working with us, we'll have to show to them that we are worth the time investment.

#### Starting as small as possible

* Very often as new contributors arriving on a new project, we end up getting overwhelmed by the amount of information to absorb to be able to successfully contribute to it, and blocked by what we don't know: unfamiliarity with the process & codebase, the opinion and priorities of the maintainers, etc.
* Mutual trust, knowledge and relationships take time to build, it's important to not overload ourselves or the maintainers by aiming too high, too fast. It's similar to the reasons why agile development exists -- to be useful, code needs to answers specific needs from users or other developers, so it requires frequent communication, and iterative changes to reflect new information you collect over time. Plan work in small short iterations, and avoid writing a lot of code at a time. 
  * You might know the saying "[commit early and commit often](https://blog.codinghorror.com/check-in-early-check-in-often/)", which recommends to create a small and frequent stream of commits while working, rather than one large infrequent commit. The idea, besides allowing to preserve a more granulary history of changes, is also to facilitate the integration of the work with other developers -- commits are a form of communication, which allows others to be aware of your current work, and reconcile theirs with yours more easily. With contribution, this is even more important, as initially existing developers are not even aware of you -- pushing code towards them, in small and frequent quantity, allows them to become aware of your work, and you of theirs through their reviews. The saying becomes "Get merges early, get merges often".

* We want to maximize the amount of feedback we get from the project, as this information will allow us to shape our contribution in a way that can be accepted by the project. Small iterations optimize this information collection process:
  * The production of concrete work incentivize the project maintainers to review and produce feedback,
  * The small size of the patch from each iteration keeps the amount of effort required from them small so they will get to it faster, 
  * Getting this feedback will allow us to approach the next set of changes more efficiently, as we will already be aware of the information provided in the last rounds of feedback.
  * Example of what not to do: In OpenStack, Hewlett-Packard forked the code base and worked for 1-2 years on a huge contribution, without frequent interaction. When it was time to be merged, it was a catastrophe of epic proportions to the point that it made the news, and its management had to acknowledge the issue.

* Keeping the size small is especially important in the initial stages, when we know very little (and don't know what we don't know), and have not yet acquired any trust from the project maintainers.
* Which gives the golden rule of the new contributor: **The size of a contribution/interaction should not exceed the total size of our previous successful contributions to the project**, building up very slowly at first from the smallest possible useful contribution.
  * Successful contribution = acknowledged and accepted by the project, either through a positive answer or a merge pull request. Pick the smallest interaction and contribution you can think of at first. The goal isn't to demonstrate technical excellence, or to show a lot of work.

#### Activity: Introducing ourselves

* We start with the smallest interaction: introducing ourselves on the selected project (and optionally in the MOOC forums too), to:
  * Thank the project contributors for their work (like in the previous activity)
  * Mention your intention of contributing,
  * Show your homework: what you have read from the project (link to the contribution guidelines & to the list of tickets for beginners if the project has some), as well as your areas of interest, and asking if there are specific small beginner tickets that would be most useful to the project right now.
  * Verify that the project answers to you
* TODO: Find and link to some good and constructive examples of how this is done well. For large projects, take into account that they might have already documented the ways to start contributing. e.g. this post for Django, https://groups.google.com/g/django-users/c/FQbpV1zcg_I/m/Pb1WhBuXDAAJ
It can still be useful to say hi even on projects with documented ways to start, as the idea is to start a conversation, to starting identify not just how to do things, but with whom (who answers?) and what (what do the people who answer care about?).
* TODO: Talk about mailing-list, forum or chat [etiquette](http://www.shakthimaan.com/downloads/glv/presentations/mailing-list-etiquette.pdf), as well as code of conduct.

* Segue into the next week: look at whether they communicate and reply on merge requests
* [Optional] Bonus to add to the introduction, to get the discussion about contributing started: "By the way, I'm interested in maybe contributing. Is there something easy that I could pick up as a first contribution?". If the documentation for new contributors mentions some tasks for beginners, it can be worth mentioning that you have looked into that list, mentioning the tasks that looked interesting to you, and asking for confirmation that they are good tasks for beginners, as well as which one(s) would be most useful to the project. This way, you'll avoid the trappings of false beginner tasks (tasks which look easy and are even categorized as such but are actually hard for a beginner), and if anyone reacts about some of these tasks, you will know about their interest, and can later get help or reviews from them.

### TODO

Tips not related to communication that have been mentionned, to include in following weeks:
* Research on the history of the project
* Presence of continuous integration, tests, etc.
* Activity: Micro-contribution
  * Right after introducing ourselves, it can help to show goodwill and start exploring the project's contribution processes by making a _micro-contribution_, which means a contribution so small and straightforward that it should not require any thinking or discussion by the reviewer (but still be useful to the project).
  * The idea here is to avoid focusing on the technical challenge, by making the contribution itself as small as possible, to be able to through the contribution process once. We want to gain knowledge about that process, both from a technical and an interpersonal point of view. It will come in handy once we start considering slightly larger contributions -- we will know what to expect.
  * Example of a microcontribution: https://github.com/python/cpython/pull/7348

## What you have learned (recap/text/video?)

Assignee: Rémi

* Checkbox list - check the learning objectives learned

## Quizz time! (10 min)

* End the module with a quiz like multiple choice questions, to evaluate whether learners have learned the learning objectives

## TODO

Ideas and tasks from brainstorming sections that haven't been incorporated in the previous sections yet:
* Give ways to "fast-forward" modules if people are already familiar with the subject
* Difference between open-source and open to contribution: https://github.com/benbjohnson/litestream#open-source-not-open-contribution
* Ask the students: What are some softwares that are important in your life? What are things that you use on a daily life, and you'd like to know the people who do it a little bit better? Or even be able to add a feature there that you like?
* Do not to over-commit because it can turn a positive experience into something negative, when we don't actually have the time to complete what we committed to. Take on little, but do it, and then take more once you're done.
  * Also, when you can't take care of commitments you have taken anymore, either because you don't have the time anymore, or the energy for it, say so. It's much better to recognize and address those situations early, before it turns into a burnout. You can always come back to it later on, when/if you want to then - you'll be able to think about it more clearly after a pause anyway.
* People coming to complain and ask to fix the bug as soon as possible -- it's important to them, and they can't understand why it wouldn't be to others, and don't want to contribute it either. I.e. being blamed like a proprietary software provider, despite the fact that the deal is very different in the case of FLOSS. The power to alter and fix the software is shared, but also the responsibility for it.
* Include tests in contributions, and properly test both automatically and manually your code before submitting, to make sure to not lose a maintainer's time if your code actually doesn't work.
* Add a section for open source project owners? "How to see if your project is fit to attract new contributors?", recapping the advices we give to contributors for evaluating open source projects. Something contributors could point to?
  * Additionally, references that provide best practices for open source projects, like https://producingoss.com/ could be used to build checklists of criterias for contributors to evaluate projects

Ideas from the [Upstream University training recording](https://gitlab.com/mooc-floss/mooc-floss/-/issues/27):
* Thinking about the upstream maintainers in positive terms is important to build trust with them -- otherwise collaboration will be extremely difficult. Even when we disagree about their methods, or find flaws in their technical approaches, it's important to maintain our respect and trust in their judgement. One way to do this is to remember how much work, effort and skill it must have taken them to get the project to what it is now. Nobody is perfect, but they have been good enough to bring the project to its current stage. They deserve merit and respect from that achievement -- while, when we join the project, we haven't yet proven our worth, nor contributed a comparable amount of work, or even fully understood the full context and history of the project, yet.
* Mentors as scrum masters: forms a team with the mentee to help further the mentee's goals, but is also part of the upstream team. Responsibilities include:
  * Removing road blocks,
  * Protecting against external interferences within the selected project,
  * Ensuring that communication channels remain open,
  * Making sure the rules of the game are followed
  * Letting the mentee and the upstream team manage itself
  * Help finding tasks that are meaningful for both the mentee's side (including the mentee's company if applicable) and the upstream side
  * Meet with the mentee regularly: daily/weekly 15 minutes standup - frequency proportional to the amount of work the mentee is able to put (every 8h of work)
